
const DEFAULT_ORDER = 4;
const DEFAULT_LENGTH = 100;

let fs = require('fs');
let fileName = process.argv[2];
let order = Number(process.argv[3]) || DEFAULT_ORDER;
let sentenceLength = Number(process.argv[4]) || DEFAULT_LENGTH;
let startWith = process.argv[5];

String.prototype.lastN = function (n) {
  return this.substring(this.length - n);
};

String.prototype.cut = function (partLength) {
  let index = 0;
  let parts = [];

  while (index + partLength < this.length) {
    parts.push(this.substring(index, index + partLength));
    index++;
  }

  return parts;
};

Array.prototype.uniq = function () {
  return this.reduce((array, element) => {
    if (array.indexOf(element) === -1) {
      array.push(element);
    }

    return array;
  }, []);
};

Array.prototype.random = function () {
  return this[~~ (this.length * Math.random())];
};

Array.prototype.first = function () {
  return this[0];
};

Array.prototype.last = function (array) {
  return this[this.length - 1];
};

fs.readFile(fileName, 'utf8', (error, data) => {
  if (error) {
    return console.error(error);
  }

  processData(data);
});

function processData (text) {
  let textParts = text.toLowerCase().cut(order);

  let tree = buildProbabilityTree(textParts);
  let startSequence = ((tree, startText) =>
    tree.textPartsStartingWith(startText).random())(tree, startWith) ||
    tree.randomTextPart();
  console.log(generateText(tree, sentenceLength, startSequence));
}

function generateText (probabilityTree, characterCount, startWith) {
  let text = probabilityTree.hasOwnProperty(startWith) ? startWith : probabilityTree.randomTextPart();

  while (text.length < characterCount) {
    let last = text.lastN(order);
    let nextCharacters = probabilityTree[last];
    if (!nextCharacters) {
      nextCharacters = probabilityTree[probabilityTree.randomTextPart()];
    }
    text += nextCharacters.random();
  }

  return text;
}

function buildProbabilityTree (textParts) {
  var tree = initTree(textParts);

  function initTree (allTextParts) {
    let tree = Object.create({
      randomTextPart  () {
        return Object.keys(this).random();
      },
      textPartsStartingWith (startText) {
        if (!startText) {
          return [];
        } else {
          return Object.keys(this).filter(value => value.startsWith(startText));
        }
      }
    });

    allTextParts.uniq().forEach((value) => {
      tree[value] = [];
    });

    return tree;
  }

  textParts.forEach((value, index, array) => {
    if (index < array.length - 1) {
      tree[value].push(array[index + 1][order - 1]);
    }
  });

  return tree;
}
